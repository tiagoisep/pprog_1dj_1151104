/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Utilitarios.Relogio;


/**
 *
 * @author Tiago Ferreira
 */
public class AcessoAreaRestrita {

    private Equipamento equipamento;
    private AreaRestrita area;
    private CartaoIdentificacao cartao;
    private Relogio time;
    private Colaborador colaborador;
    private boolean acessoEPermitido;
    private String movimento;

    public AcessoAreaRestrita(Equipamento equipamento, AreaRestrita area, CartaoIdentificacao cartao, Colaborador colaborador, Relogio time, boolean acessoEPermitido, String movimento) {
        this.equipamento = equipamento;
        this.area = area;
        this.cartao = cartao;
        this.colaborador = colaborador;
        this.time = time;
        this.acessoEPermitido = acessoEPermitido;
        this.movimento = movimento;
    }

    /**
     * @return the equipamento
     */
    public Equipamento getEquipamento() {
        return equipamento;
    }

    /**
     * @return the area
     */
    public AreaRestrita getArea() {
        return area;
    }

    /**
     * @return the cartao
     */
    public CartaoIdentificacao getCartao() {
        return cartao;
    }

    /**
     * @return the time
     */
    public Relogio getTime() {
        return time;
    }

    /**
     * @return the colaborador
     */
    public Colaborador getColaborador() {
        return colaborador;
    }

    /**
     * @return the acessoEPermitido
     */
    public boolean isAcessoEPermitido() {
        return acessoEPermitido;
    }

    /**
     * @return the movimento
     */
    public String getMovimento() {
        return movimento;
    }

    
}
