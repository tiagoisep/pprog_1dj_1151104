/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Utilitarios.Relogio;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Ferreira
 */
public class RegistoAcessos {

    /**
     * Lista de Acessos a Areas Restritas
     */
    private List<AcessoAreaRestrita> listaAcessos = new ArrayList<>();

    /**
     * Acesso a uma área restrita.
     */
    private AcessoAreaRestrita acessoArea;

    /**
     * Construtor de RegistoAcessos
     */
    public RegistoAcessos() {
        listaAcessos = new ArrayList<>();
    }

    /**
     * Cria um novo acesso a Area Restrita
     * 
     * @param equipamento
     * @param area
     * @param cartao
     * @param colaborador
     * @param time
     * @param acessoEPermitido
     * @param movimento
     * @return 
     */
    public AcessoAreaRestrita novoAcesso(Equipamento equipamento, AreaRestrita area, CartaoIdentificacao cartao, 
            Colaborador colaborador, Relogio time, boolean acessoEPermitido, String movimento) {

        return new AcessoAreaRestrita(equipamento, area, cartao, colaborador, time, acessoEPermitido, movimento);
    }

    /**
     * Adiciona um acesso à Lista de Acessos
     * @param acesso
     * @return 
     */
    public boolean addAcesso(AcessoAreaRestrita acesso) {
        return listaAcessos.add(acesso);
    }
}
