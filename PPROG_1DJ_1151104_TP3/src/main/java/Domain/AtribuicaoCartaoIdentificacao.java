/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.sql.Time;
import java.util.Date;



/**
 *
 * @author Tiago Ferreira
 */
public class AtribuicaoCartaoIdentificacao {
    
    /**
     * O Colaborador
     */
    private Colaborador colaborador;
    /**
     * A data da atribuicao
     */
    private Date data;
    /**
     * O Cartao que foi atribuido ao Colaborador
     */
    private CartaoIdentificacao cartao;

    /**
     * Construtor completo de AtribuicaoCartaoIdentificacao
     * 
     * @param colaborador
     * @param data
     * @param cartao 
     */
    public AtribuicaoCartaoIdentificacao(Colaborador colaborador, Date data, CartaoIdentificacao cartao) {
        this.colaborador = colaborador;
        this.data = data;
        this.cartao = cartao;
    }
    
    /**
     * Construtor vazio de AtribuicaoCartaoIdentificacao
     */
    public AtribuicaoCartaoIdentificacao() {
        
    }

    /**
     * Devolve o Colaborador
     * 
     * @return o colaborador
     */
    public Colaborador getColaborador() {
        return colaborador;
    }

    /**
     * Devolve a data de atribuicao
     * 
     * @return a data
     */
    public Date getData() {
        return data;
    }

    /**
     * Devolve o Cartao de Identificacao atribuido
     * 
     * @return o cartao
     */
    public CartaoIdentificacao getCartao() {
        return cartao;
    }

    /**
     * Devolve a descrição textual da Atribuicao de Cartao de Identificacao a um Colaborador.
     * @return 
     */
    @Override
    public String toString() {
        return "AtribuicaoCartaoIdentificacao{" + "colaborador=" + colaborador + ", data=" + data + ", cartao=" + cartao + '}';
    }
    
    

}