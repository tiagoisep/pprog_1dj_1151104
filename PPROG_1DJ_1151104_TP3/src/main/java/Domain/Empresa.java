/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 *
 * @author Tiago Ferreira
 */
public class Empresa {

    private RegistoAcessos registoAcessos;
    private RegistoCartoes registoCartoes;
    private RegistoColaboradores registoColaboradores;
    private RegistoEquipamentos registoEquipamentos;
    private RegistoPerfilAutorizacao registoPerfilAutorizacao;
    
    /**
     * Construtor de Empresa
     */
    public Empresa() {
        this.registoAcessos = new RegistoAcessos();
        this.registoCartoes = new RegistoCartoes();
        this.registoColaboradores = new RegistoColaboradores();
        this.registoEquipamentos = new RegistoEquipamentos();
        this.registoPerfilAutorizacao = new RegistoPerfilAutorizacao();
    }

    /**
     * Devolve o Registo de Acessos da Empresa
     * 
     * @return o registoAcessos
     */
    public RegistoAcessos getRegistoAcessos() {
        return registoAcessos;
    }

    /**
     * Devolve o Registo de Cartoes da Empresa
     * 
     * @return o registoCartoes
     */
    public RegistoCartoes getRegistoCartoes() {
        return registoCartoes;
    }

    /**
     * Devolve o Registo de Colaboradores da Empresa
     * 
     * @return the registoColaboradores
     */
    public RegistoColaboradores getRegistoColaboradores() {
        return registoColaboradores;
    }

    /**
     * Devolve o Registo de Equipamentos da Empresa
     * 
     * @return o registoEquipamentos
     */
    public RegistoEquipamentos getRegistoEquipamentos() {
        return registoEquipamentos;
    }

    /**
     * Devolve o Registo de Perfis de Autorização da Empresa
     * 
     * @return the registoPerfilAutorizacao
     */
    public RegistoPerfilAutorizacao getRegistoPerfilAutorizacao() {
        return registoPerfilAutorizacao;
    }

    /**
     * Modifica o Registo de Acessos da Empresa
     * 
     * @param registoAcessos o novo registoAcessos
     */
    public void setRegistoAcessos(RegistoAcessos registoAcessos) {
        this.registoAcessos = registoAcessos;
    }

    /**
     * Modifica o Registo de Cartoes da Empresa
     * 
     * @param registoCartoes o novo registoCartoes
     */
    public void setRegistoCartoes(RegistoCartoes registoCartoes) {
        this.registoCartoes = registoCartoes;
    }

    /**
     * Modifica o Registo de Colaboradores da Empresa
     * 
     * @param registoColaboradores o novo registoColaboradores
     */
    public void setRegistoColaboradores(RegistoColaboradores registoColaboradores) {
        this.registoColaboradores = registoColaboradores;
    }

    /**
     * Modifica o Registo de Equipamentos da Empresa
     * 
     * @param registoEquipamentos o novo registoEquipamentos
     */
    public void setRegistoEquipamentos(RegistoEquipamentos registoEquipamentos) {
        this.registoEquipamentos = registoEquipamentos;
    }

    /**
     * Modifica o Registo de Autorizacao da Empresa
     * 
     * @param registoPerfilAutorizacao o novo registoPerfilAutorizacao
     */
    public void setRegistoPerfilAutorizacao(RegistoPerfilAutorizacao registoPerfilAutorizacao) {
        this.registoPerfilAutorizacao = registoPerfilAutorizacao;
    }

}
