/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Ferreira
 */
public class RegistoPerfilAutorizacao {

    /**
     * Lista de perfis de autorização
     */
    private List<PerfilAutorizacao> listaPerfisAutorizacao;
    /**
     * instancia de perf auto
     */
    private PerfilAutorizacao perfil;

    /**
     * constroi uma instancia de um registo de perfil de autorização vazio
     */
    public RegistoPerfilAutorizacao() {
        this.listaPerfisAutorizacao = new ArrayList<>();
    }

    /**
     * @return the listaPerfisAutorizacao
     */
    public List<PerfilAutorizacao> getListaPerfisAutorizacao() {
        return listaPerfisAutorizacao;
    }

    /**
     * cria novo perfil de autorização
     *
     * @return novo perfil de autorização
     */
    public PerfilAutorizacao novoPerfilDeAutorizacao() {
        return new PerfilAutorizacao();
    }

    /**
     *
     * valida se a lista de perfil contem um perfil de autorização p
     *
     * @param p perfil de autorização
     * @return
     */
    public boolean validaExistencia(PerfilAutorizacao p) {
        return listaPerfisAutorizacao.contains(p);
    }

    public boolean validaDados(PerfilAutorizacao p) {
        if (p == null) {
            return false;
        }
        return true;
    }

    /**
     * regista um perfil de autorização
     *
     * @param p perfil de autorização
     * @return
     */
    public boolean registarPerfilAutorizacao(PerfilAutorizacao p) {
        if (validaExistencia(p) == false) {
            return addPerfil(p);
        }
        return false;
    }

    /**
     * Adiciona o perfil p à lista
     *
     * @param p perfil de autorização
     * @return
     */
    private boolean addPerfil(PerfilAutorizacao p) {
        return listaPerfisAutorizacao.add(p);
    }
    
    /**
     * Procura o Perfil de Autorizacao pelo id em String
     * 
     * @param id
     * @return o perfil se encontrar
     *         null se não encontrar
     */
    public PerfilAutorizacao getPerfilPorId(String id) {
        for (PerfilAutorizacao perfil : listaPerfisAutorizacao) {
            if (perfil.getId().equalsIgnoreCase(id)) {
                return perfil;
            }
        }
        return null;
    }

}
