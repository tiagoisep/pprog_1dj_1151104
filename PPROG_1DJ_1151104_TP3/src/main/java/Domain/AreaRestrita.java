/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.Objects;

/**
 *
 * @author Tiago Ferreira
 */
public class AreaRestrita {
    
    /**
     * O codigo da Area Restrita
     */
    private String codigo;
    /**
     * A descrição da Area Restrita
     */
    private String descricao;
    /**
     * A localizacao da Area Restrita
     */
    private String localizacao;
    /**
     * A lotação máxima da Area Restrita
     */
    private int lotacao;

    /**
     * Construtor completo de AreaRestrita
     * 
     * @param codigo
     * @param descricao
     * @param localizacao
     * @param lotacao 
     */
    public AreaRestrita(String codigo, String descricao, String localizacao, int lotacao) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.localizacao = localizacao;
        this.lotacao = lotacao;
    }

    /**
     * Contrutor vazio de AreaRestrita
     */
    public AreaRestrita() {
    }

    /**
     * Devolve o codigo da Area Restrita
     * 
     * @return o codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Devolve a descrição da Area Restrita
     * 
     * @return a descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Devolve a localização da Area Restrita
     * 
     * @return a localizacao
     */
    public String getLocalizacao() {
        return localizacao;
    }

    /**
     * Devolve o valor da lotacao maxima da Area Restrita
     * 
     * @return a lotacao maxima
     */
    public int getLotacao() {
        return lotacao;
    }

    /**
     * Modifica o codigo da Area Restrita
     * 
     * @param codigo o novo codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Modifica a descricao da Area Restrita
     * 
     * @param descricao a nova descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Modifica a localização da Area Restrita
     * 
     * @param localizacao a nova localizacao
     */
    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    /**
     * Modifica o valor da lotacao da Area Restrita
     * 
     * @param lotacao o novo valor da lotacao
     */
    public void setLotacao(int lotacao) {
        this.lotacao = lotacao;
    }

    /**
     * Descrição textual de AreaRestrita
     * @return 
     */
    @Override
    public String toString() {
        return "AreaRestrita{" + "codigo=" + codigo + ", descricao=" + descricao + ", localizacao=" + localizacao + ", lotacao=" + lotacao + '}';
    }

    /**
     * Metodo Equals restrito de forma a comparar o codigo de duas AreasRestritas
     * @param obj
     * @return true se ambas as areas restritas tiverem o mesmo codigo
     *         false se não.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AreaRestrita outroAreaRestrita = (AreaRestrita) obj;
        return this.codigo.equalsIgnoreCase(outroAreaRestrita.codigo);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.codigo);
        return hash;
    }
}
