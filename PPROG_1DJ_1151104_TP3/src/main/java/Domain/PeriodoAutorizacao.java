/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Utilitarios.Relogio;

/**
 *
 * @author Tiago Ferreira
 */
public class PeriodoAutorizacao {

    /**
     * O Equipamento do Periodo de Autorizacao
     */
    private Equipamento equipamento;
    /**
     * O dia da semana do Periodo de Autorizacao
     */
    private String diaSemana;
    /**
     * A hora de inicio do Periodo de Autorizacao
     */
    private Relogio tempoInicio;
    /**
     * A hora de fim do Periodo de Autorizacao
     */
    private Relogio tempoFim;

    /**
     * Construtor completo de PeriodoAutorizacao
     * 
     * @param equipamento
     * @param diaSemana
     * @param tempoInicio
     * @param tempoFim 
     */
    public PeriodoAutorizacao(Equipamento equipamento, String diaSemana, Relogio tempoInicio, Relogio tempoFim) {
        this.equipamento = equipamento;
        this.diaSemana = diaSemana;
        this.tempoInicio = tempoInicio;
        this.tempoFim = tempoFim;
    }

    /**
     * Construtor vazio de PeriodoAutorizacao
     */
    public PeriodoAutorizacao() {
    }

    /**
     * Devolve o Equipamento do Periodo de Autorizacao
     * 
     * @return o equipamento
     */
    public Equipamento getEquipamento() {
        return equipamento;
    }
    
    /**
     * Devolve o dia da semana de um Periodo de Autorizacao
     * 
     * @return o diaSemana
     */
    public String getDiaSemana() {
        return diaSemana;
    }

    /**
     * Modifica o dia da semana de um Periodo de Autorizacao
     * 
     * @param diaSemana o novo diaSemana
     */
    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    /**
     * Devolve a hora de inicio de um Periodo de Autorizacao
     * 
     * @return o tempoInicio
     */
    public Relogio getTempoInicio() {
        return tempoInicio;
    }

    /**
     * Modifica a hora de inicio de um Periodo de Autorizacao
     * 
     * @param tempoInicio o novo tempoInicio
     */
    public void setTempoInicio(Relogio tempoInicio) {
        this.tempoInicio = tempoInicio;
    }

    /**
     * Devolve a hora de fim de um Periodo de Autorizacao
     * 
     * @return o tempoFim
     */
    public Relogio getTempoFim() {
        return tempoFim;
    }

    /**
     * Modifica a hora de fim de um Periodo de Autorizacao
     * 
     * @param tempoFim o novo tempo Fim
     */
    public void setTempoFim(Relogio tempoFim) {
        this.tempoFim = tempoFim;
    }

    /**
     * Devolve a descrição textual de um Periodo de Autorizacao
     * @return 
     */
    @Override
    public String toString() {
        return "Equipamento"+getEquipamento() +"disponivel "+diaSemana+ ", das " + tempoInicio + ",até às" + tempoFim;
    }

}
