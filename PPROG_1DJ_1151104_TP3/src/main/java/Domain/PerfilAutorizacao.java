/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Utilitarios.Relogio;

/**
 *
 * @author Tiago Ferreira
 */
public class PerfilAutorizacao {

    /**
     * O ID do Perfil de Autorizacao
     */
    private String id;
    /**
     * A descricao do Perfil de Autorizacao
     */
    private String descricao;
    /**
     * A lista de Periodos de Autorizacao
     */
    private ListaPeriodoAutorizacao listaPeriodoAutorizacao = new ListaPeriodoAutorizacao();
    /**
     * O Periodo de Autorizaco
     */
    private PeriodoAutorizacao pa;

    /**
     * Construtor completo de PerfilAutorizacao
     *
     * @param id
     * @param descricao
     */
    public PerfilAutorizacao(String id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    /**
     * Construtor vazio de PerfilAutorizacao
     */
    public PerfilAutorizacao() {

    }

    /**
     * Devolve o ID do Perfil de Autorizacao
     *
     * @return o id
     */
    public String getId() {
        return id;
    }

    /**
     * Devolve a descricao do Perfil de Autorizacao
     *
     * @return a descricao
     */
    public String getDescricao() {
        return descricao;
    }
    
    /**
     * Devolve a Lista de Periodos de Autorizacao
     * 
     * @return a listaPeriodoAutorizacao
     */
    public ListaPeriodoAutorizacao getListaPeriodoAutorizacao() {
        return listaPeriodoAutorizacao;
    }

    /**
     * Modifica o ID do Perfil de Autorizacao
     *
     * @param id o novo id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Modifica a descricao do Perfil de Autorizacao
     *
     * @param descricao a nova descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Devolve a descrição textual de um Perfil de Autorizacao
     *
     * @return
     */
    @Override
    public String toString() {
        return "PerfilAutorizacao{" + "id=" + id + ", descricao=" + descricao + '}';
    }
    

    /**
     * Adiciona periodo à lista de Periodos de Autorizacao
     *
     * @param equipamento
     * @param dia
     * @param tempoInicio
     * @param tempoFim
     * @return true se adicionou false se não adicionou
     */
    public boolean adicionaPeriodo(Equipamento equipamento, String dia, Relogio tempoInicio, Relogio tempoFim) {
        pa = listaPeriodoAutorizacao.criaPeriodoAutorizacao(equipamento, dia, tempoInicio, tempoFim);
        return listaPeriodoAutorizacao.adicionarPeriodoAutorizacao(pa);
    }

    

    /**
     * Valida se o Perfil de Autorizacao não é null
     *
     * @param p
     * @return true se o Perfil de Autorizacao tem dados lá dentro
     *         false se for null
     */
    public boolean valida(PerfilAutorizacao p) {
        return p != null;
    }

}
