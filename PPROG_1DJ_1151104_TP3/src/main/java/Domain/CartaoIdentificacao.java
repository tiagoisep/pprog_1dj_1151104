/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Tiago Ferreira
 */
public class CartaoIdentificacao {

    /**
     * O ID do Cartao de Identificação
     */
    private String id;
    /**
     * A data de Emissão do Cartao de Identificação
     */
    private Date dataEmissao;
    /**
     * A versão do Cartao de Identificação
     */
    private String versao;
    /**
     * Lista de Atribuicoes de Cartoes de Identificacao
     */
    List<AtribuicaoCartaoIdentificacao> listaAtribuicoes = new ArrayList<>();

    /**
     * Construtor completo de Cartao de Identificacao
     * 
     * @param id
     * @param dataEmissao
     * @param versao 
     */
    public CartaoIdentificacao(String id, Date dataEmissao, String versao) {
        this.id = id;
        this.dataEmissao = dataEmissao;
        this.versao = versao;
    }

    /**
     * Construtor vazio de Cartao de Identificação
     */
    public CartaoIdentificacao() {
    }

    /**
     * Devolve o ID do Cartao de Identificação
     * 
     * @return o id
     */
    public String getId() {
        return id;
    }

    /**
     * Modifica o ID do Cartao de Identificação
     * 
     * @param id o novo id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Devolve a data de emissao do Cartao de Identificação
     * 
     * @return a dataEmissao
     */
    public Date getDataEmissao() {
        return dataEmissao;
    }

    /**
     * Modifica a data de emissao do Cartao de Identificação
     * 
     * @param dataEmissao a nova dataEmissao
     */
    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    /**
     * Devolve a versao do Cartao de Identificação
     * 
     * @return a versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Modifica a versao do Cartao de Identificação
     * 
     * @param versao a nova versao
     */
    public void setVersao(String versao) {
        this.versao = versao;
    }

    /**
     * Devolve a descrição textual do Cartao de Identificação
     * @return 
     */
    @Override
    public String toString() {
        return "ID:" + id + ", dataEmissao: " + dataEmissao + ", versao: " + versao;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    /**
     * Metodo Equals rescrito para comparar o id de 2 cartões de identificação.
     * 
     * @param obj
     * @return true se for o mesmo id
     *         false se não for
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CartaoIdentificacao outroCartao = (CartaoIdentificacao) obj;
        return this.id.equalsIgnoreCase(outroCartao.id);
    }
    
    /**
     * Valida se o Cartao de Identificação não é null
     * 
     * @param cartao
     * @return true se não for null
     *         false se for
     */
    public boolean valida(CartaoIdentificacao cartao) {
        return cartao != null;
    }
    
    public boolean adicionaAtribuicao(AtribuicaoCartaoIdentificacao atribuicao) {
        return listaAtribuicoes.add(atribuicao);
    }

    public Colaborador getColaboradorAtribuido(CartaoIdentificacao cartao) {
        for (AtribuicaoCartaoIdentificacao atribuicao : listaAtribuicoes) {

            if (atribuicao.getCartao().equals(cartao)) {
                return atribuicao.getColaborador();
            }
        }
        return null;
    }

}
