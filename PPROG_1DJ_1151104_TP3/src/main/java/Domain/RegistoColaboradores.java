/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Ferreira
 */
public class RegistoColaboradores {

    /**
     * A Lista dos colaboradores
     */
    private List<Colaborador> listaColaboradores;

    /**
     * Um Colaborador.
     */
    private Colaborador colaborador;

    /**
     * Construtor completo: cria uma instância de RegistoColaboradores.
     */
    public RegistoColaboradores() {
        listaColaboradores = new ArrayList<>();
    }

    /**
     * Método que cria uma nova instância de Colaborador.
     *
     * @param numeroMecanografico
     * @param nomeCompleto
     * @param nomeAbreviado
     * @param perfil
     * @return Colaborador
     */
    public Colaborador novoColaborador(String numeroMecanografico, String nomeCompleto, String nomeAbreviado, PerfilAutorizacao perfil) {
        return new Colaborador(numeroMecanografico, nomeCompleto, nomeAbreviado, perfil);
    }

    /**
     * Valida se a lista contem um Colaborador
     *
     * @param colaborador
     * @return verdadeiro ou falso
     */
    public boolean valida(Colaborador colaborador) {
        return listaColaboradores.contains(colaborador);
    }

    /**
     * Adiciona um Colaborador à Lista de Colaboradores.
     *
     * @param colaborador
     */
    public void addColaborador(Colaborador colaborador) {
        listaColaboradores.add(colaborador);

    }

    /**
     * Procura um Colaborador pelo seu numero mecanografico
     *
     * @param numeroMecanografico
     * @return o Colaborador se encontrar 
     *         null se não
     */
    public Colaborador getColaboradorPorNumero(String numeroMecanografico) {
        for (Colaborador colaborador : listaColaboradores) {
            if (colaborador.getNumeroMecanografico().equalsIgnoreCase(numeroMecanografico)) {
                return colaborador;
            }
        }
        return null;
    }

}
