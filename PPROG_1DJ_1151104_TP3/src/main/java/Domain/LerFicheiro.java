/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Utilitarios.Relogio;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Tiago Ferreira
 */
public class LerFicheiro {

    private static final String SEPARADOR = ";";
    private static Scanner lerFicheiro;

    public static Empresa lerFicheiro(Empresa empresa) throws FileNotFoundException {

        RegistoEquipamentos registoEquipamentos = new RegistoEquipamentos();
        RegistoColaboradores registoColaboradores = new RegistoColaboradores();
        RegistoCartoes registoCartoes = new RegistoCartoes();
        RegistoPerfilAutorizacao registoPerfis = new RegistoPerfilAutorizacao();
        
        List<AreaRestrita> listaAreasRestritas = new ArrayList<>();
        
        lerFicheiro = new Scanner(new File("dados.txt"));
        //Lê cabeçalho AreaRestrita
        lerFicheiro.nextLine();
        
    //Leitura das Areas Restritras
        String key = "Equipamento";
        lerAreaRestrita(key, listaAreasRestritas);

    //Leitura dos Equipamentos
        key = "CartaoIdentificacao";
        lerEquipamento(key, registoEquipamentos, listaAreasRestritas);

    //Leitura dos Cartoes de Identificacao;
        key = "PerfilAutorizacao";
        lerCartaoIdentificacao(key, registoCartoes);

    //Leitura do Perfil de Autorizacao
        key = "PeriodoAutorizacao";
        lerPerfilAutorizacao(key, registoPerfis);

    //Leitura do Periodo Autorizacao
        key = "Colaborador";
        lerPeriodoAutorizacao(key, registoEquipamentos, registoPerfis);

    //Leitura do Colaborador
        key = "AtribuicaoCartaoIdentificacao";
        lerColaborador(key, registoColaboradores, registoPerfis);

    //Leitura da Atribuicao de Cartao de Identificacao
        lerAtribuicaoCartaoIdentificacao(registoColaboradores, registoCartoes);
        
        lerFicheiro.close();
        empresa.setRegistoCartoes(registoCartoes);
        empresa.setRegistoColaboradores(registoColaboradores);
        empresa.setRegistoEquipamentos(registoEquipamentos);
        empresa.setRegistoPerfilAutorizacao(registoPerfis);
        
        return empresa;
    }

    /**
     * Leitura de todas as AreasRestritas.
     * Termina quando encontrar o cabeçalho da próxima leitura - "Equipamento"
     * 
     * @param id
     * @param listaAreasRestritas 
     */
    public static void lerAreaRestrita(String id, List<AreaRestrita> listaAreasRestritas) {
        while (lerFicheiro.hasNext()) {
            String linha = lerFicheiro.nextLine();
            if (linha.length() > 0) {
                if (linha.equalsIgnoreCase(id)) {
                    break;
                }
                String[] temp = linha.trim().split(SEPARADOR);
                int lotacao = Integer.parseInt(temp[3]);
                AreaRestrita area = new AreaRestrita(temp[0], temp[1], temp[2], lotacao);
                listaAreasRestritas.add(area);
            }
        }
    }

    /**
     * Leitura de todos os Equipamentos.
     * Termina quando encontrar o cabeçalho da próxima leitura - "CartaoIdentificacao"
     * 
     * @param id
     * @param registoEquipamentos
     * @param listaAreasRestritas 
     */
    public static void lerEquipamento(String id, RegistoEquipamentos registoEquipamentos, List<AreaRestrita> listaAreasRestritas) {
        while (lerFicheiro.hasNext()) {
            String linha = lerFicheiro.nextLine();
            if (linha.length() > 0) {
                if (linha.equalsIgnoreCase(id)) {
                    break;
                }
                String[] temp = linha.trim().split(SEPARADOR);
                String codigoArea = temp[2];
                AreaRestrita area = procuraArea(codigoArea, listaAreasRestritas);
                Equipamento eq = new Equipamento(temp[0], temp[1], area, temp[3]);
                registoEquipamentos.addEquipamento(eq);
            }
        }
    }

    /**
     * Leitura de todos os Cartoes de Identificacao
     * Termina quando encontrar o cabeçalho da próxima leitura - PerfilAutorizacao
     * 
     * @param id
     * @param registoCartoes 
     */
    public static void lerCartaoIdentificacao(String id, RegistoCartoes registoCartoes) {
        while (lerFicheiro.hasNext()) {
            String linha = lerFicheiro.nextLine();
            if (linha.length() > 0) {
                if (linha.equalsIgnoreCase(id)) {
                    break;
                }
                String[] temp = linha.trim().split(SEPARADOR);
                Date dataEmissao = new Date(temp[1]);
                CartaoIdentificacao cartao = new CartaoIdentificacao(temp[0], dataEmissao, temp[2]);
                registoCartoes.registaCartao(cartao);
            }
        }
    }

    /**
     * Leitura de todos os Perfis de Autorizacao
     * Termina quando encontrar o cabeçalho da próxima leitura - "PeriodoAutorizacao"
     * 
     * @param id
     * @param registoPerfilAutorizacao 
     */
    public static void lerPerfilAutorizacao(String id, RegistoPerfilAutorizacao registoPerfilAutorizacao) {
        while (lerFicheiro.hasNext()) {
            String linha = lerFicheiro.nextLine();
            if (linha.length() > 0) {
                if (linha.equalsIgnoreCase(id)) {
                    break;
                }
                String[] temp = linha.trim().split(SEPARADOR);
                PerfilAutorizacao p = new PerfilAutorizacao(temp[0], temp[1]);
                registoPerfilAutorizacao.registarPerfilAutorizacao(p);
            }
        }
    }

    /**
     * Leitura de Periodos de Autorizacao
     * Termina quando encontrar o cabeçalho da próxima leitura - Colaborador
     * 
     * @param id
     * @param registoEquipamentos 
     * @param registoPerfilAutorizacao 
     */
    public static void lerPeriodoAutorizacao(String id, RegistoEquipamentos registoEquipamentos, RegistoPerfilAutorizacao registoPerfilAutorizacao) {
        while (lerFicheiro.hasNext()) {
            String linha = lerFicheiro.nextLine();
            if (linha.length() > 0) {
                if (linha.equalsIgnoreCase(id)) {
                    break;
                }
                String[] temp = linha.trim().split(SEPARADOR);
                
                String idPerfil = temp[0];
                PerfilAutorizacao perfil = registoPerfilAutorizacao.getPerfilPorId(idPerfil);
                
                String equip = temp[1];
                Equipamento equipamento = registoEquipamentos.getEquipamentoByID(equip);
                
                String dia = temp[2];
                
                String[] tempo1 = temp[3].split(":");
                int hora = Integer.parseInt(tempo1[0]);
                int minuto = Integer.parseInt(tempo1[1]);
                
                Relogio horaInicio = new Relogio(hora, minuto);
                
                String[] tempo2 = temp[4].split(":");
                int h = Integer.parseInt(tempo2[0]);
                int m = Integer.parseInt(tempo2[1]);
                
                Relogio horaFim = new Relogio(h, m);
                
                PeriodoAutorizacao pa = new PeriodoAutorizacao(equipamento, dia, horaInicio, horaFim);
                
                ListaPeriodoAutorizacao listaPeriodoAutorizacao = perfil.getListaPeriodoAutorizacao();
                listaPeriodoAutorizacao.adicionarPeriodoAutorizacao(pa);
            }
        }
    }

    /**
     * Leitura de todos os Colaboradores
     * Termina quando encontra o próximo cabeçalho a ler - AtribuicaoCartaoIdentificacao
     * 
     * @param id
     * @param registoColaboradores 
     * @param registoPerfilAutorizacao 
     */
    public static void lerColaborador(String id, RegistoColaboradores registoColaboradores, RegistoPerfilAutorizacao registoPerfilAutorizacao) {
        while (lerFicheiro.hasNext()) {
            String linha = lerFicheiro.nextLine();
            if (linha.length() > 0) {
                if (linha.equalsIgnoreCase(id)) {
                    break;
                }
                String[] temp = linha.trim().split(SEPARADOR);
                String idPerfil = temp[3];
                PerfilAutorizacao perfil = registoPerfilAutorizacao.getPerfilPorId(idPerfil);
                Colaborador colaborador = new Colaborador(temp[0], temp[1], temp[2], perfil);
                registoColaboradores.addColaborador(colaborador);
            }
        }
    }

    /**
     * Leitura de todas as Atribuicoes de Cartoes de Identificacao a Colaboradores.
     * 
     * @param registoColaboradores
     * @param registoCartoes
     */
    public static void lerAtribuicaoCartaoIdentificacao(RegistoColaboradores registoColaboradores, RegistoCartoes registoCartoes) {
        while (lerFicheiro.hasNext()) {
            String linha = lerFicheiro.nextLine();
            if (linha.length() > 0) {
                String[] temp = linha.trim().split(SEPARADOR);
                
                String colabID = temp[0];
                Colaborador colaborador = registoColaboradores.getColaboradorPorNumero(colabID);
                
                Date data = new Date(temp[1]);
                
                String cartaoID = temp[2];
                CartaoIdentificacao cartao = registoCartoes.getCartaoByID(cartaoID);
                
                AtribuicaoCartaoIdentificacao atribuicao = new AtribuicaoCartaoIdentificacao(colaborador, data, cartao);
                cartao.listaAtribuicoes.add(atribuicao);

            }
        }
    }

    /**
     * Procura uma Area Restrita pelo seu codigo em String
     * 
     * @param codigoArea
     * @param listaAreaRestrita
     * @return a Area Restrita se encontrar
     *         null se não encontrar
     */
    private static AreaRestrita procuraArea(String codigoArea, List<AreaRestrita> listaAreaRestrita) {
        for (AreaRestrita area : listaAreaRestrita) {
            if(area.getCodigo().equalsIgnoreCase(codigoArea)){
                return area;
            }
        }
        return null;
    }
}
