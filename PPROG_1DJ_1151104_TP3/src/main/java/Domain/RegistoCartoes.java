/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Tiago Ferreira
 */
public class RegistoCartoes {
    
    /**
     * A Lista de Cartões de Identificação
     */
    private List<CartaoIdentificacao> listaCartoesIdentificacao;
    /**
     * Um Cartão de Identificação
     */
    private CartaoIdentificacao cartao;

    /**
     * Construtor completo: cria uma instância de RegistoCartoes.
     */
    public RegistoCartoes() {
        listaCartoesIdentificacao = new ArrayList<>();
    }

    /**
     * Método que cria uma nova instância de Cartão.
     *
     * @param id
     * @param dataEmissao
     * @param versao
     * @return 
     */
    public CartaoIdentificacao novoCartao(String id, Date dataEmissao, String versao) {
        return new CartaoIdentificacao(id, dataEmissao, versao);
    }

    /**
     * Valida Cartao
     * 
     * @param cartao
     * @return
     */
//    public boolean validaCartao(CartaoIdentificacao cartao) {
//        return cartao.valida(cartao);
//    }

    /**
     * Método que permite registar um cartão, caso ainda não exita na
     * lista de cartões.
     * @param cartao
     * @return true caso possa ser adicionado
     *         false caso não possa ser adicionado
     */
    public boolean registaCartao(CartaoIdentificacao cartao) {
        if (!listaCartoesIdentificacao.contains(cartao)) {
            return addCartao(cartao);
        }
        return false;
    }

    /**
     * Adiciona o Cartão de Identificação à Lista de Cartões.
     *
     * @param cartao
     */
    private boolean addCartao(CartaoIdentificacao cartao) {
        return listaCartoesIdentificacao.add(cartao);
    }
    
    /**
     * Procura um Cartao de Identificacao pelo ID
     * 
     * @param IdCartao
     * @return o CartaoIdentificacao se encontrou
     *         null se não encontrou
     */
    public CartaoIdentificacao getCartaoByID(String IdCartao){
       
        for (CartaoIdentificacao cartao : listaCartoesIdentificacao) {
            if(cartao.getId().equals(IdCartao)){
                return cartao;
            }
        }
        return null;
    }
    
}
