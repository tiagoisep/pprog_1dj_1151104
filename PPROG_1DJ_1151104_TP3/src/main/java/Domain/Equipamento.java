/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.Objects;

/**
 *
 * @author Tiago Ferreira
 */
public class Equipamento {

    /**
     * O Endereço Logico do Equipamento
     */
    private String enderecoLogico;
    /**
     * A descrição do Equipamento
     */
    private String descricao;
    /**
     * A Area Restrita controlada pelo Equipamento
     */
    private AreaRestrita areaRestrita;
    /**
     * O movimento controlado pelo Equipamento.
     */
    private String movimento;

    /**
     * O Construtor completo de Equipamento
     *
     * @param enderecoLogico
     * @param descricao
     * @param areaRestrita
     * @param movimento
     */
    public Equipamento(String enderecoLogico, String descricao, AreaRestrita areaRestrita, String movimento) {
        this.enderecoLogico = enderecoLogico;
        this.descricao = descricao;
        this.areaRestrita = areaRestrita;
        this.movimento = movimento;
    }

    /**
     * Construtor vazio de Equipamento
     */
    public Equipamento() {
    }

    /**
     * Devolve o endereço logico do Equipamento
     *
     * @return o enderecoLogico
     */
    public String getEnderecoLogico() {
        return enderecoLogico;
    }

    /**
     * Devolve a descricao do Equipamento
     *
     * @return a descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Devolve a area restrita do Equipamento
     *
     * @return a areaRestrita
     */
    public AreaRestrita getAreaRestrita() {
        return areaRestrita;
    }

    /**
     * Devolve o movimento do Equipamento
     *
     * @return o movimento
     */
    public String getMovimento() {
        return movimento;
    }

    /**
     * Modifica o endereço logico do Equipamento
     *
     * @param enderecoLogico o novo enderecoLogico
     */
    public void setEnderecoLogico(String enderecoLogico) {
        this.enderecoLogico = enderecoLogico;
    }

    /**
     * Modifica a descricao do Equipamento
     *
     * @param descricao a nova descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Modifica o movimento do Equipamento
     *
     * @param movimento o novo movimento
     */
    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    /**
     * Devolve a descrição textual de um Equipamento
     *
     * @return
     */
    @Override
    public String toString() {
        return "Equipamento{" + "enderecoLogico=" + enderecoLogico + ", descricao=" + descricao + ", areaRestrita=" + areaRestrita + ", movimento=" + movimento + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.enderecoLogico);
        return hash;
    }

    /**
     * Metodo Equals rescrito para comparar o endereço lógico de 2 Equipamentos
     *
     * @param obj
     * @return true se for o mesmo endereço lógico false se não for.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Equipamento outroEquip = (Equipamento) obj;
        return this.enderecoLogico.equalsIgnoreCase(outroEquip.enderecoLogico);
    }

}
