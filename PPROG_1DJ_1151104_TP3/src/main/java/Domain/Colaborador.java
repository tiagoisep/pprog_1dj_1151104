/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.Objects;

/**
 *
 * @author Tiago Ferreira
 */
public class Colaborador {

    /**
     * O número mecanografico do Colaborador
     */
    private String numeroMecanografico;
    /**
     * O nome completo do Colaborador
     */
    private String nomeCompleto;
    /**
     * O nome abreviado do Colaborador
     */
    private String nomeAbreviado;
    /**
     * O Perfil de Autorização do Colaborador
     */
    private PerfilAutorizacao perfil;

    /**
     * O construtor completo de um Colaborador
     * 
     * @param numeroMecanografico
     * @param nomeCompleto
     * @param nomeAbreviado 
     * @param perfil 
     */
    public Colaborador(String numeroMecanografico, String nomeCompleto, String nomeAbreviado, PerfilAutorizacao perfil) {
        this.numeroMecanografico = numeroMecanografico;
        this.nomeCompleto = nomeCompleto;
        this.nomeAbreviado = nomeAbreviado;
        this.perfil = perfil;
    }

    /**
     * Construtor vazio de um Colaborador
     */
    public Colaborador() {
    }

    /**
     * Devolve o numero mecanografico do Colaborador
     * 
     * @return o numeroMecanografico
     */
    public String getNumeroMecanografico() {
        return numeroMecanografico;
    }

    /**
     * Devolve o nome completo do Colaborador
     * 
     * @return o nomeCompleto
     */
    public String getNomeCompleto() {
        return nomeCompleto;
    }

    /**
     * Devolve o nome abreviado do Colaborador
     * 
     * @return o nomeAbreviado
     */
    public String getNomeAbreviado() {
        return nomeAbreviado;
    }

    /**
     * Devolve o perfil do Colaborador
     * 
     * @return o perfil
     */
    public PerfilAutorizacao getPerfil() {
        return perfil;
    }

    /**
     * Modifica o numero mecanografico do Colaborador
     * 
     * @param numeroMecanografico o novo numeroMecanografico
     */
    public void setNumeroMecanografico(String numeroMecanografico) {
        this.numeroMecanografico = numeroMecanografico;
    }

    /**
     * Modifica o nome completo do Colaborador
     * 
     * @param nomeCompleto o novo nomeCompleto
     */
    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    /**
     * Modifica o nome abreviado do Colaborador
     * 
     * @param nomeAbreviado o novo nomeAbreviado
     */
    public void setNomeAbreviado(String nomeAbreviado) {
        this.nomeAbreviado = nomeAbreviado;
    }

    /**
     * Modifica o Perfil de Autorizacao do Colaborador
     * 
     * @param perfil o novo perfil
     */
    public void setPerfil(PerfilAutorizacao perfil) {
        this.perfil = perfil;
    }

    /**
     * Devolve a descrição textual de um Colaborador
     * @return 
     */
    @Override
    public String toString() {
        return "Colaborador{" + "numeroMecanografico=" + numeroMecanografico + ", nomeCompleto=" + nomeCompleto + ", nomeAbreviado=" + nomeAbreviado + ", perfil=" + perfil + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.numeroMecanografico);
        return hash;
    }

    /**
     * Metodo Equals rescrito para comparar os numeros mecanograficos de 2 Colaboradores
     * @param obj
     * @return true se é o mesmo numero mecanografico
     *         false se não é
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Colaborador outroColaborador = (Colaborador) obj;
        return this.numeroMecanografico.equalsIgnoreCase(outroColaborador.numeroMecanografico);
    }

    
    
}