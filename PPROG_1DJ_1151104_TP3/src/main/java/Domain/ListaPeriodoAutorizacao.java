/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Utilitarios.Relogio;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Ferreira
 */
public class ListaPeriodoAutorizacao {

    /**
     * Lista de Periodos de Autorização
     */
    private List<PeriodoAutorizacao> listaPeriodosAutorizacao;

    /**
     * Período de Autorização
     */
    private PeriodoAutorizacao pa;

    /**
     * Construtor de ListaPeriodoAutorizacao
     */
    public ListaPeriodoAutorizacao() {
        this.listaPeriodosAutorizacao = new ArrayList<>();
    }

    /**
     * Devolve a Lista de Periodos de Autorizacao
     *
     * @return a listaPeriodosAutorizacao
     */
    public List<PeriodoAutorizacao> getListaPeriodosAutorizacao() {
        return listaPeriodosAutorizacao;
    }

    /**
     * Cria um novo Periodo de Autorizacao
     *
     * @param equipamento
     * @param dia
     * @param tempoInicio
     * @param tempoFim
     * @return
     */
    public PeriodoAutorizacao criaPeriodoAutorizacao(Equipamento equipamento, String dia, Relogio tempoInicio, Relogio tempoFim) {
        return new PeriodoAutorizacao(equipamento, dia, tempoInicio, tempoFim);
    }

    /**
     * Adiciona um Periodo de Autorizacao à Lista de Periodos de Autorizacao
     *
     * @param pa
     * @return true se adicionar
     *         false se não
     */
    public boolean adicionarPeriodoAutorizacao(PeriodoAutorizacao pa) {
        return listaPeriodosAutorizacao.add(pa);
    }

    /**
     * Método que verifica se uma determinada hora está contida num periodo.
     *
     * @param periodo
     * @param tempo
     * @return true caso esteja contida false caso não esteja contida
     */
    public boolean contemHora(PeriodoAutorizacao periodo, Relogio tempo) {
        int tempoInicio = converteTempoEmMinutos(periodo.getTempoInicio());
        int tempoFim = converteTempoEmMinutos(periodo.getTempoFim());
        int tempoATestar = converteTempoEmMinutos(tempo);
        return (tempoInicio <= tempoATestar && tempoFim >= tempoATestar);
    }

    /**
     * Converte o objeto Hora em tempo (minutos).
     *
     * @param hora
     * @return minutos (inteiro)
     */
    public int converteTempoEmMinutos(Relogio hora) {
        int minutos = hora.getHora() * 60;
        int total = minutos + hora.getMinutos();
        return total;
    }
    
    /**
     * Valida um Periodo de Autorizacao
     *
     * @param p
     * @return true se for valido
     *         false se não
     */
    public boolean validaPeriodo(PeriodoAutorizacao p) {
        
        for (PeriodoAutorizacao periodoAutorizacao : listaPeriodosAutorizacao) {
            if (periodoAutorizacao.getEquipamento().equals(p.getEquipamento())) {
                if (periodoAutorizacao.getDiaSemana().equalsIgnoreCase(p.getDiaSemana())) {

                    Relogio tempoInicio = p.getTempoInicio();
                    Relogio tempoFim = p.getTempoFim();
                    
                    boolean validaTempoInicio = contemHora(periodoAutorizacao, tempoInicio);
                    boolean validaTempoFim = contemHora(periodoAutorizacao, tempoFim);
                    
                    if (validaTempoInicio || validaTempoFim) {
                        return false;
                    } else if (converteTempoEmMinutos(p.getTempoInicio()) < converteTempoEmMinutos(p.getTempoInicio())
                            && converteTempoEmMinutos((p.getTempoFim())) > converteTempoEmMinutos(p.getTempoFim())) {
                        return false;
                    }
                    

                    
                }
            }
        }
        return true;
    }
    
    /**
     * Valida se um determinado determinado Equipamento tem autorizacao de acesso num determinado dia e numa determinada hora
     * 
     * @param equipamento
     * @param dia
     * @param tempoAtual
     * @return 
     */
    public boolean validaAcessoEquipamento(Equipamento equipamento, String dia, Relogio tempoAtual) {

        for (PeriodoAutorizacao cadaPeriodo : listaPeriodosAutorizacao) {
            if (cadaPeriodo.getEquipamento().equals(equipamento)) {
                if(cadaPeriodo.getDiaSemana().equalsIgnoreCase(dia)){
                    int minutosAgora = converteTempoEmMinutos(tempoAtual);
                    int minutosInicio = converteTempoEmMinutos(cadaPeriodo.getTempoInicio());
                    int minutosFim = converteTempoEmMinutos(cadaPeriodo.getTempoFim());
                    
                    if (minutosInicio < minutosAgora && minutosFim > minutosAgora){
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
