/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Ferreira
 */
public class RegistoEquipamentos {

    /**
     * A Lista de Equipamentos
     */
    private List<Equipamento> listaEquipamentos;

    /**
     * Construtor de RegistoEquipamentos
     */
    public RegistoEquipamentos() {
        this.listaEquipamentos = new ArrayList<>();
    }

    /**
     * Devolve a Lista de Equipamentos
     * @return 
     */
    public List<Equipamento> getListaEquipamentos() {
        return listaEquipamentos;
    }

    /**
     * Procura um Equipamento pelo seu ID.
     * 
     * @param id
     * @return Equipamento se encontrar
     *         null se não
     */
    public Equipamento getEquipamentoByID(String id) {
        for (Equipamento equipamento : listaEquipamentos) {
            if (equipamento.getEnderecoLogico().equalsIgnoreCase(id)) {
                return equipamento;
            }
        }
        return null;
    }

    /**
     * Adiciona um Equipamento à Lista de Equipamentos
     * 
     * @param equip
     * @return true se adicionou
     *         false se não
     */
    public boolean addEquipamento(Equipamento equip) {
        return listaEquipamentos.add(equip);
    }

}
