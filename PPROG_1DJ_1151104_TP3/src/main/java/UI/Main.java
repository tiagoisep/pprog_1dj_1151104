/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Domain.AreaRestrita;
import Domain.AtribuicaoCartaoIdentificacao;
import Domain.CartaoIdentificacao;
import Domain.Colaborador;
import Domain.Empresa;
import Domain.Equipamento;
import Domain.ListaPeriodoAutorizacao;
import Domain.PerfilAutorizacao;
import Domain.PeriodoAutorizacao;
import Domain.RegistoCartoes;
import Domain.RegistoColaboradores;
import Domain.RegistoEquipamentos;
import Domain.RegistoPerfilAutorizacao;
import Utilitarios.Relogio;
import java.util.Date;

/**
 *
 * @author Tiago Ferreira
 */
public class Main {

    public static void main(String[] args) {
        try {
            Empresa empresa = new Empresa();
//            AreaRestrita area = new AreaRestrita("B123", "lalala", "ISEP", 4);
//            
//            Equipamento equip = new Equipamento("abc", "badabada", area, "f");
//            Equipamento equip2 = new Equipamento("def", "wgwgwf", area, "f");
//            Equipamento equip3 = new Equipamento("qwe", "cwdc", area, "f");
//            Equipamento equip4 = new Equipamento("mnb", "hhgr",  area, "f");
//            
//            RegistoEquipamentos requip= new RegistoEquipamentos();
//            requip.addEquipamento(equip);
//            requip.addEquipamento(equip2);
//            requip.addEquipamento(equip3);
//            requip.addEquipamento(equip4);
//            empresa.setRegistoEquipamentos(requip);
//            
//            PerfilAutorizacao p1 = new PerfilAutorizacao("111", "fuxe");
//            RegistoPerfilAutorizacao regper = new RegistoPerfilAutorizacao();
//            regper.registarPerfilAutorizacao(p1);
//            empresa.setRegistoPerfilAutorizacao(regper);
//            
//            Colaborador colaborador = new Colaborador("1100816", "Diogo", "D", p1);
//            RegistoColaboradores regCol = new RegistoColaboradores();
//            regCol.addColaborador(colaborador);
//            empresa.setRegistoColaboradores(regCol);
//            
//            Date dataEmissao = new Date(20117, 5, 8);
//            CartaoIdentificacao cartao = new CartaoIdentificacao("777", dataEmissao, "1");
//            RegistoCartoes regCartoes = new RegistoCartoes();
//            regCartoes.registaCartao(cartao);
//            empresa.setRegistoCartoes(regCartoes);
//            
//            AtribuicaoCartaoIdentificacao aci = new AtribuicaoCartaoIdentificacao(colaborador, dataEmissao, cartao);
//            cartao.adicionaAtribuicao(aci);
//            
//            int inicio = 22;
//            int fim1 = 00;
//            int fim2 = 30;
//            
//            Relogio horaInicio = new Relogio(inicio, fim1);
//            Relogio horaFim = new Relogio(inicio, fim2);
//            PeriodoAutorizacao pa1 = new PeriodoAutorizacao(equip, "Sunday", horaInicio, horaFim);
//            ListaPeriodoAutorizacao lpa = new ListaPeriodoAutorizacao();
//            lpa.adicionarPeriodoAutorizacao(pa1);
            
            MenuUI uiMenu = new MenuUI(empresa);
            uiMenu.run();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
