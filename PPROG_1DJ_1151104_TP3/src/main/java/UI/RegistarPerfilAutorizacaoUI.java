/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.RegistarPerfilAutorizacaoController;
import Domain.Empresa;
import Domain.Equipamento;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Tiago Ferreira
 */
public class RegistarPerfilAutorizacaoUI {

    private static Scanner read = new Scanner(System.in);

    private final Empresa empresa;

    private final RegistarPerfilAutorizacaoController rpaController;

    /**
     * Constroi uma instancia de RegistarPerfilAutorizacaoUI com a empresa
     * passada por parametro e o Controller inicializado.
     *
     * @param empresa
     */
    public RegistarPerfilAutorizacaoUI(Empresa empresa) {
        this.empresa = empresa;
        this.rpaController = new RegistarPerfilAutorizacaoController(empresa);
    }

    public void run() {
        
        List<String> diasDaSemana = new ArrayList<>();
            diasDaSemana.add("Monday");
            diasDaSemana.add("Tuesday");
            diasDaSemana.add("Wednesday");
            diasDaSemana.add("Thursday");
            diasDaSemana.add("Friday");
            diasDaSemana.add("Saturday");
            diasDaSemana.add("Sunday");
        
        //Inicia registo
        rpaController.novoPerfilAutorizacao();

        //Solicita dados(identificador e descricao)
        // e introduz dados(id e desc)
        System.out.println("Dados para o Perfil de Autorização:\n\n");
        
        System.out.println("Pff introduza o identificador");
        String id = read.nextLine();
        
        System.out.println("\nPff introduza a descricao");
        String descricao = read.nextLine();
        
        rpaController.setDados(id, descricao);

        // mostra equipamentos e solicita equipamentos a autorizar
        System.out.println("\nLista de Equipamentos:");
        List<Equipamento> listaEquip = rpaController.getListaEquipamentos();
        imprimeEquipamentos(listaEquip);

        /**
         * while1 introduz equipamento
         *
         * solicita periodo autorizacao while2 introduz periodo autorizacao
         */
        System.out.println("\npor favor escolha o equipamento ou digite 'fim' para terminar");
        String equipID = read.nextLine();
        while (!equipID.equalsIgnoreCase("fim")) {
            rpaController.getEquipamento(equipID);

            System.out.println("por favor introduza os dados do periodo de autorizacao ou '0' para terminar");

            System.out.println("Dia da semana: ");
            String diaSemana = read.nextLine();

            while (!diaSemana.equalsIgnoreCase("0")) {

                System.out.println("Hora de inicio: ");
                String tempoInicio = read.nextLine();

                System.out.println("Hora de fim: ");
                String tempoFim = read.nextLine();

                rpaController.addPeriodo(diaSemana, tempoInicio, tempoFim);

                System.out.println("por favor introduza os dados do periodo de autorizacao ou '0' para terminar");
                System.out.println("Dia da semana: ");
                diaSemana = read.nextLine();
            }
            System.out.println("\nLista de Equipamentos:");
            imprimeEquipamentos(listaEquip);
            System.out.println("\npor favor escolha o equipamento ou digite 'fim' para terminar");
            equipID = read.nextLine();
        }

        /**
         * apresenta dados e solicita confirmacao confirma sucesso
         */
        System.out.println("São estes os seus dados?? (s/n)");
        String dados = rpaController.MostraDados();
        System.out.println(dados);
        String confirmacao = read.nextLine();
        if (confirmacao.equalsIgnoreCase("s")) {
            rpaController.RegistaDados();
            System.out.println("Dados Registados Com sucesso!");
        } else {
            System.out.println("Cancelado");
        }
    }
    
    /**
     * Imprime os Equipamentos do RegistoEquipamentos
     * 
     * @param listaEquipamentos 
     */
    private void imprimeEquipamentos(List<Equipamento> listaEquipamentos){
        for (Equipamento equipamento : listaEquipamentos) {
            System.out.println(equipamento);
        }
    }
    
    /**
     * Imprime os dias da Semana
     * 
     * @param listaDias 
     */
    private void imprimeDias(List<String> listaDias){
        for (String dia : listaDias) {
            System.out.println(dia.indexOf(dia) + ". "  +dia);
        }
    }
    

}
