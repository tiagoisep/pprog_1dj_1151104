/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AcederAreaRestritaController;
import Domain.Empresa;
import java.util.Scanner;
import java.time.LocalDateTime;

/**
 *
 * @author Tiago Ferreira
 */
public class AcederAreaRestritaUI {
 
    private static Scanner read = new Scanner(System.in);
    
    private final Empresa empresa;

    private final AcederAreaRestritaController aarController;

    public AcederAreaRestritaUI(Empresa empresa) {
        this.empresa = empresa;
        this.aarController = new AcederAreaRestritaController(empresa);
    }
    
    
    public void run(){
    
    /**
     * solicita apresentacao do cartao
     * apresenta cartao
     * permite acesso
     */
    
        System.out.println("Pff introduza o identificador do Equipamento: ");
        String idEquip = read.nextLine();

        System.out.println("Pff introduza o número do cartão: ");
        String idCartao = read.nextLine();

        int hora = LocalDateTime.now().getHour();
        int min = LocalDateTime.now().getMinute();

        String time = hora + ":" + min;

        boolean accessoBemSucedido = aarController.solicitaAcesso(idEquip, idCartao, time);

        if (accessoBemSucedido == true) {
            System.out.println("Acesso bem sucedido!");
        } else {
            System.out.println("Acesso negado!");
        }
    
    }
}
