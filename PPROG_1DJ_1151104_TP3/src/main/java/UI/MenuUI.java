/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Domain.Empresa;
import Domain.LerFicheiro;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Tiago Ferreira
 */
class MenuUI {

    private static Scanner read = new Scanner(System.in);
    private Empresa m_empresa;
    private String opcao;

    public MenuUI(Empresa empresa) {
        m_empresa = empresa;
    }

    public void run() throws IOException {

        do {
            System.out.println("\n\nSoftware de Controlo de presenças");

            System.out.println("\nMenu:");

            System.out.println("1 - Registar Perfil de Autorização");
            System.out.println("2 - Aceder a Área Restrita");
            System.out.println("3 - Carregar dados de ficheiro");
            System.out.println("0 - Sair");
            System.out.println("\nSelecione uma opção por favor");
            opcao = read.nextLine();
            switch (opcao) {
                case "1":
                    RegistarPerfilAutorizacaoUI uiRPA = new RegistarPerfilAutorizacaoUI(m_empresa);
                    uiRPA.run();
                    break;
                case "2":
                    AcederAreaRestritaUI uiAAR = new AcederAreaRestritaUI(m_empresa);
                    uiAAR.run();
                    break;
                case "3":
                    m_empresa = LerFicheiro.lerFicheiro(m_empresa);
                    //LeituraFicheiroUI uiLF = new LeituraFicheiroUI();
                    System.out.println("\nA leitura do ficheiro foi bem sucedida.");
                    break;
                case "0":
                    System.out.println("A sessão foi terminada com sucesso");
                    break;
            }
        } while (!opcao.equals("0"));

    }
}
