/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilitarios;

/**
 *
 * @author Tiago Ferreira
 */
public class Relogio {
    
    /**
     * Uma hora.
     */
    private int hora;
    /**
     * Um minuto
     */
    private int minutos;

    /**
     * Construtor de Relogio com horas e minutos
     * 
     * @param hora
     * @param minutos 
     */
    public Relogio(int hora, int minutos) {
        this.hora = hora;
        this.minutos = minutos;
    }
    
    /**
     * Construtor vazio 
     */
    public Relogio(){
        
    }

    /**
     * Devolve a hora.
     * 
     * @return a hora
     */
    public int getHora() {
        return hora;
    }

    /**
     * Modifica a hora do Relogio.
     * 
     * @param hora a nova hora
     */
    public void setHora(int hora) {
        this.hora = hora;
    }

    /**
     * Devolve os minutos do Relogio
     * 
     * @return os minutos
     */
    public int getMinutos() {
        return minutos;
    }

    /**
     * Modifica os minutos do Relogio
     * 
     * @param minutos os novos minutos
     */
    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    /**
     * Devolve a descrição textual do Relogio
     * @return 
     */
    @Override
    public String toString() {
        return hora + ":" + minutos;
    }
    
    
}
