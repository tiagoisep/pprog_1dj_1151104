/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilitarios;

import Domain.Empresa;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author LerFicheiroBinController
 */
public class EscreverFicheiroBin {
    public static void Serialize(Empresa empresa) {

        try {
            FileOutputStream fileOut = new FileOutputStream("Serializable.dat");

            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(empresa);
            out.close();
            fileOut.close();
            System.out.printf("Serialize foi guardado em Serializable.dat");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
}
