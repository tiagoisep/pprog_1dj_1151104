/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilitarios;

import Domain.Empresa;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author Tiago Ferreira
 */
public class LerFicheiroBin {
    
    public static Empresa Deserialize() {
        Empresa empresa;
        try {
            FileInputStream fileIn = new FileInputStream("Serializable.dat");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            empresa = (Empresa) in.readObject();
            in.close();
            fileIn.close();
            return empresa;
        } catch (IOException i) {
            i.printStackTrace();
            return null;
        } catch (ClassNotFoundException c) {
            System.out.println("Não foi possivel encontrar conteúdo.");
            c.printStackTrace();
            return null;
        }
    }
}
