/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Domain.AcessoAreaRestrita;
import Domain.AreaRestrita;
import Domain.CartaoIdentificacao;
import Domain.Colaborador;
import Domain.Empresa;
import Domain.Equipamento;
import Domain.ListaPeriodoAutorizacao;
import Domain.PerfilAutorizacao;
import Domain.RegistoAcessos;
import Domain.RegistoCartoes;
import Domain.RegistoEquipamentos;
import Utilitarios.Relogio;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;

/**
 *
 * @author Tiago Ferreira
 */
public class AcederAreaRestritaController {
    
    private final Empresa empresa;

    private RegistoEquipamentos registoEquip;
    
    private Equipamento equipamento;
    
    private RegistoCartoes registoCart;
    
    private CartaoIdentificacao cartao;
    
    private Colaborador colaborador;
    
    private PerfilAutorizacao perfilAutorizacao;
    
    private RegistoAcessos registoAcess;
    
    private ListaPeriodoAutorizacao listaPeriodoAutorizacao;
    
    private AreaRestrita area;
    
    public AcederAreaRestritaController(Empresa empresa) {
        this.empresa = empresa;
    }

    public boolean solicitaAcesso(String idEquip, String idCartao, String tempo) {

        registoEquip = empresa.getRegistoEquipamentos();
        equipamento = registoEquip.getEquipamentoByID(idEquip);

        registoCart = empresa.getRegistoCartoes();
        cartao = registoCart.getCartaoByID(idCartao);

        colaborador = cartao.getColaboradorAtribuido(cartao);

        perfilAutorizacao = colaborador.getPerfil();

        listaPeriodoAutorizacao = perfilAutorizacao.getListaPeriodoAutorizacao();

        String dia = LocalDateTime.now().getDayOfWeek().toString();
        
        String [] temp = tempo.split(":");
        int hora = Integer.parseInt(temp[0]);
        int minutos = Integer.parseInt(temp[1]);
        
        Relogio time = new Relogio(hora, minutos);
        
        boolean acessoEPermitido = listaPeriodoAutorizacao.validaAcessoEquipamento(equipamento, dia, time);

        area = equipamento.getAreaRestrita();
        String movimento = equipamento.getMovimento();
        
        registoAcess = empresa.getRegistoAcessos();

        AcessoAreaRestrita acesso = registoAcess.novoAcesso(equipamento, area, cartao, colaborador, time,
                acessoEPermitido, movimento);
        
        registoAcess.addAcesso(acesso);
        if (acessoEPermitido) {
            return true;
        }
        
        return false;
    }
}
