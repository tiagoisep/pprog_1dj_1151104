/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Domain.Empresa;
import Domain.Equipamento;
import Domain.ListaPeriodoAutorizacao;
import Domain.PerfilAutorizacao;
import Domain.PeriodoAutorizacao;
import Domain.RegistoEquipamentos;
import Domain.RegistoPerfilAutorizacao;
import Utilitarios.Relogio;
import java.util.List;

/**
 *
 * @author Tiago Ferreira
 */
public class RegistarPerfilAutorizacaoController {

    /**
     * Instancia de Empresa
     */
    private final Empresa empresa;

    /**
     * Instancia de Registo de Perfil de autorização
     */
    private RegistoPerfilAutorizacao registoPerf;
    /**
     * Instancia de registo de equipamentos
     */
    private RegistoEquipamentos registoEquip;
    /**
     * Instancia de perfil de autorizacao
     */
    private PerfilAutorizacao perfAuto;

    /**
     * Instancia de periodo de autorizacao
     *
     */
    private PeriodoAutorizacao periodoAuto;

    /**
     * Instancia de Periodo de Autorizacao
     */
    private ListaPeriodoAutorizacao listaPeriodoAutorizacao;
    /**
     * Instancia de equipamentos
     */
    private Equipamento equipamento;

    /**
     * /**
     * Constroi uma instancia de RegistarPerfilAutorizacaoController com a
     * empresa passada por parametro
     *
     * @param empresa
     */
    public RegistarPerfilAutorizacaoController(Empresa empresa) {
        this.empresa = empresa;
    }

    public void novoPerfilAutorizacao() {
        registoPerf = empresa.getRegistoPerfilAutorizacao();
        perfAuto = registoPerf.novoPerfilDeAutorizacao();
    }

    public void setDados(String id, String descricao) {
        perfAuto.setId(id);
        perfAuto.setDescricao(descricao);
    }

    public List<Equipamento> getListaEquipamentos() {
        registoEquip = empresa.getRegistoEquipamentos();
        return registoEquip.getListaEquipamentos();
    }

    public void getEquipamento(String id) {
        equipamento = registoEquip.getEquipamentoByID(id);
    }

    public void addPeriodo(String dia, String tempoInicio, String tempoFim) {
        Relogio inicio = converteStringEmRelogio(tempoInicio);
        Relogio fim = converteStringEmRelogio(tempoFim);
        perfAuto.adicionaPeriodo(equipamento, dia, inicio, fim);
    }

    public String MostraDados() {
        return this.perfAuto.toString();
    }

    public void RegistaDados() {
        registoPerf.registarPerfilAutorizacao(perfAuto);
    }

    private Relogio converteStringEmRelogio(String tempo) {
        String[] temp = tempo.trim().split(":");
        int inicio = Integer.parseInt(temp[0]);
        int fim = Integer.parseInt(temp[1]);
        return new Relogio(inicio, fim);
    }

}
